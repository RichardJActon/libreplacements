# libreplacements

> Libre (free as in freedom) replacements for proprietary (closed source) software.

[![CC BY-SA 4.0][cc-by-sa-shield]][cc-by-sa]

To make the list, a piece of software must meet the [definition of free software](https://www.gnu.org/philosophy/free-sw.html) as defined by the Free Software Foundation.

## Table of contents

- [libreplacements](#libreplacements)
  - [Table of contents](#table-of-contents)
  - [License](#license)
  - [General resources](#general-resources)
    - [other lists](#other-lists)
    - [hosting providers](#hosting-providers)
  - [Replacements by use case](#replacements-by-use-case)
    - [academic publishing](#academic-publishing)
    - [accounting/finance](#accountingfinance)
    - [animation](#animation)
    - [app stores](#app-stores)
    - [audio streaming](#audio-streaming)
    - [audio/music editing](#audiomusic-editing)
    - [authentication](#authentication)
    - [automation](#automation)
    - [back up](#back-up)
    - [blogging/publishing](#bloggingpublishing)
    - [Bittorrent client](#bittorrent-client)
    - [bookmark management](#bookmark-management)
    - [calendaring](#calendaring)
    - [cloud storage](#cloud-storage)
    - [chat (audio)](#chat-audio)
    - [chat (text)](#chat-text)
    - [chat (video conferencing)](#chat-video-conferencing)
    - [collaborative drawing](#collaborative-drawing)
    - [commenting platform](#commenting-platform)
    - [customer relationship management (CRM)](#customer-relationship-management-crm)
    - [databases](#databases)
    - [data repositories](#data-repositories)
    - [data visualisation](#data-visualisation)
    - [desktop publishing](#desktop-publishing)
    - [digital asset management (DAM)](#digital-asset-management-dam)
    - [discussion forums](#discussion-forums)
    - [ebooks](#ebooks)
    - [educational](#educational)
      - [electronics](#electronics)
      - [language](#language)
      - [physics](#physics)
    - [email](#email)
    - [encryption](#encryption)
    - [events management](#events-management)
    - [file-sending](#file-sending)
    - [fonts](#fonts)
    - [graphics](#graphics)
      - [diagrams](#diagrams)
      - [raster](#raster)
      - [vector](#vector)
    - [group collaboration platform](#group-collaboration-platform)
    - [integrated development environment (IDE)](#integrated-development-environment-ide)
    - [internet reclamation projects](#internet-reclamation-projects)
      - [hosted](#hosted)
      - [local](#local)
    - [internet speed test](#internet-speed-test)
    - [lab notebooks](#lab-notebooks)
    - [library management](#library-management)
    - [link aggregator](#link-aggregator)
    - [mailing lists and marketing](#mailing-lists-and-marketing)
    - [maps](#maps)
    - [news/feed/RSS reader](#newsfeedrss-reader)
    - [notes management](#notes-management)
    - [office suites](#office-suites)
    - [pastebins text/data-sharing](#pastebins-textdata-sharing)
    - [PDF tools](#pdf-tools)
    - [personal finance](#personal-finance)
    - [photo gallery](#photo-gallery)
    - [podcasts](#podcasts)
      - [hosting/publishing](#hostingpublishing)
    - [polling/voting](#pollingvoting)
    - [point of sales](#point-of-sales)
    - [presentations](#presentations)
      - [keystroke visualiser](#keystroke-visualiser)
    - [project management](#project-management)
    - [reference/bibliography management](#referencebibliography-management)
    - [remote access](#remote-access)
    - [screenshots/screencasts/live streaming/annotation](#screenshotsscreencastslive-streamingannotation)
    - [search engines](#search-engines)
    - [speech](#speech)
    - [spelling and grammar checker](#spelling-and-grammar-checker)
    - [spreadsheets](#spreadsheets)
    - [surveys](#surveys)
    - [tasks/todo list management (Kanban)](#taskstodo-list-management-kanban)
    - [text editors](#text-editors)
    - [time tracking/management](#time-trackingmanagement)
    - [translation](#translation)
    - [two-factor authentication (2FA)](#two-factor-authentication-2fa)
    - [URL/link shorteners](#urllink-shorteners)
    - [user interface (UI) \& user experience (UX) mockups](#user-interface-ui--user-experience-ux-mockups)
    - [video editors](#video-editors)
    - [web analytics/monitoring](#web-analyticsmonitoring)
    - [web browser](#web-browser)
    - [word processing](#word-processing)

## License

This work is licensed under the [Creative Commons Attribution-ShareAlike 4.0
International License][cc-by-sa].

[![CC BY-SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: http://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://licensebuttons.net/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg

## General resources

### other lists

* [Alternative Internet](https://redecentralize.github.io/alternative-internet/)
* [Android FOSS](https://github.com/offa/android-foss) list for Android
* [awesome-humane-tech](https://github.com/humanetech-community/awesome-humane-tech) with comprehensive list of not just free software, but also other informative resources and communities
  * [Humane Tech forums](https://community.humanetech.com/)
* [awesome-oss-alternatives](https://github.com/RunaCapital/awesome-oss-alternatives) SASS replacements
* [awesome-selfhosted](https://github.com/Kickball/awesome-selfhosted) links of self-hosted replacements
* [awesome-sysadmin](https://github.com/n1trux/awesome-sysadmin)
* [Awesome Robotic Tooling](https://freerobotics.tools/) is an awesome list of free software tools related to robotics but also other contexts such as computer vision, data processing/analyses, networking, IDEs, and many other things
* [deGoogle](https://degoogle.jmoore.dev/) is a "huge list of alternatives to Google products. Privacy tips, tricks, and links."
  * Useful resource, but doesn't stricly filter out proprietary solutions
* [Go FOSS](https://gofoss.net/)
  * Hacker News [thread](https://news.ycombinator.com/item?id=34388095)
* [Hacker News meta thread](https://news.ycombinator.com/item?id=31589924) with links to multiple lists
* [libresaas](http://libresaas.org/) has a list of self-hostable software
* [Mr Free Tools](https://mrfreetools.com/tools/?_free_type=open-source) has a category for free software tools
* [No More Google](https://nomoregoogle.com/) (not all options are libre, they are just non-Google)
* [opensource.builders](https://opensource.builders/) is another list of free software replacements, more related to the needs of software developers
* [Remote communication](https://libreplanet.org/wiki/Remote_Communication) tools list hosted on [LibrePlanet](https://libreplanet.org/)
* [switching.software](https://switching.software/) is a good list of mostly free software replacements
  * Impressively, they included a list of replacements for the notorious Google reCAPTCHA!
  
### hosting providers

* [awesome-librehosters](https://framagit.org/librehosters/awesome-librehosters) is a curated list of groups hosting services based on free software
* [CHATONS](https://entraide.chatons.org/en/) hosts free software webservices such as pads, a file sender, or image sharer, etc.
* [elestio](https://elest.io/) is a commercial hosting provider for deploying 150+ open source apps/services on dedicated virtual machines.
* [fairkom](https://www.fairkom.eu/en) hosts free-of-charge and paid hosting for common services from pads to chat.
* [Framasoft](https://framasoft.org/en/) maintains a meta-list of libre web services on [this page](https://alt.framasoft.org/en/)
* The [libre hosters network](https://libreho.st/) is a network of hosters of free (as in freedom) web services. Notably:
  * [Cloud68](https://cloud68.co/) offers paid hosting of services (e.g. BigBlueButton and MediaWiki) for organisations.
  * [Snopyta](https://snopyta.org/) hosts an instance of the [Invidious](https://github.com/omarroth/invidious) YouTube proxy.
* [Libre Projects](http://www.libreprojects.net/)
* [Nomagic](https://www.nomagic.uk/services/) provides a lot of services
* [systemli.org](https://www.systemli.org/en/service/index.html) is a tech collective providing hosted services that are free software

## Replacements by use case

### academic publishing

* [MiniConf](http://www.mini-conf.org/index.html) "is a virtual conference in a box. It manages the papers, schedules, and speakers for an academic conference run virtually. It can be easily integrated with interactive tools such as video, chat, and QA."
  * Hacker News [thread](https://news.ycombinator.com/item?id=23282113)
* [Octopus](https://github.com/ScienceOctopus)
* [Open Journal System (OJS)](https://pkp.sfu.ca/ojs) by Uniquity Press et al.
* [Janeway](https://janeway.systems/) Developed by Professor Martin Eve, Mauro Sanchez and Andy Byers at the Centre for Technology and Publishing, Birkbeck, University of London, and the Open Library of Humanities, UK. 
* [Pubpub](https://www.pubpub.org/) by Knowledge Futures Group
* [Pubsweet](https://coko.foundation/product-suite) by the Coko Foundation

Thanks to @unixjazz for suggesting OJS, Pubpub, and Pubsweet.

### accounting/finance

* [FrappeBooks](https://frappebooks.com/)
  * Hacker News [thread](https://news.ycombinator.com/item?id=23091005)

### animation

* [Animatize](https://github.com/panphora/animatize) allows you to "drag a character with your mouse to create an animation"
  * Hacker News [thread](https://news.ycombinator.com/item?id=29924666) and [thread](https://news.ycombinator.com/item?id=28601192)
* [Animotion](https://github.com/animotionjs/animotion) is a visual CSS animation app
  * Hacker News [thread](https://news.ycombinator.com/item?id=38402776)
* [enve](https://maurycyliebner.github.io/) 2D animation creator
* [Glaxnimate](https://glaxnimate.mattbas.org/) is a simple and fast vector graphics animation program
* [Theatre.js](https://github.com/theatre-js/theatre) "is an animation library for high-fidelity motion graphics... [that] can be used both programmatically *and* visually".
  * Hacker News [thread](https://news.ycombinator.com/item?id=30631993) on animations
* [Wick](https://www.wickeditor.com/) is great for creating animations and multimedia projects
* [Synfig](https://www.synfig.org/) open source 2D animation

### app stores

* [F-droid](https://f-droid.org/)
* [IzzyOnDroid](https://android.izzysoft.de/articles.php)

### audio streaming

* [Balena-sound](https://github.com/balenalabs/balena-sound) is a "single or multi-room streamer for an existing audio device using a Raspberry Pi [supporting] Bluetooth, Airplay and Spotify Connect".
  * Hacker News [thread](https://news.ycombinator.com/item?id=25188496)

### audio/music editing

* [Ardour](https://www.ardour.org/) for professional audio recording, mixing, and editing and well-known for its financial sustainability
* [Audacity](https://audacityteam.org/)
  * [Wavacity](https://wavacity.com/) is a FOSS port of Audacity to the web
    * Hacker News [thread](https://news.ycombinator.com/item?id=37356497)
* [Astrofox](https://astrofox.io/) generates motion graphics from input audio: "Watch as shapes and effects pulse, rotate and move to the beat of your music."
  * Hacker News [thread](https://news.ycombinator.com/item?id=32018459)
* [Bintracker](https://bintracker.org/) chiptune audio workstation
  * Hacker News [thread](https://news.ycombinator.com/item?id=37377529)
* [Furnace](https://github.com/tildearrow/furnace) is a multi-system chiptune tracker
* [Helm](https://github.com/mtytel/helm) synthesizer
  * Hacker News [thread](https://news.ycombinator.com/item?id=38414033)
* ["Massive List of 65+ Open Source Music Production Tools"](https://midination.com/free-music-production-software/) ([wb](https://web.archive.org/web/20200804221636/https://midination.com/free-music-production-software/))
  * Hacker News [thread](https://news.ycombinator.com/item?id=23909138)
* [mayavoz](https://github.com/shahules786/mayavoz) is a "Pytorch-based opensource toolkit for speech enhancement" such as noise reduction
* [Spleeter](https://github.com/deezer/spleeter) music source separation engine
  * Hacker News [thread](https://news.ycombinator.com/item?id=23228539)
  * Additional [thread](https://news.ycombinator.com/item?id=24748793) with other resources
* [Stargate](https://stargateaudio.github.io/) is a digital audio workstation, similar to Ardour
* [Vital](https://github.com/mtytel/vital) spectral warping wavetable synth
* [KX Studio](https://kx.studio/) plugins, patchbays etc. good for live audio processing pipelines

### authentication

* [Authelia](https://github.com/authelia/authelia) is an open-source authentication/authorization server with 2FA/SS
  * Hacker News [thread](https://news.ycombinator.com/item?id=26409820)

### automation

Including home automation.

* [Mycodo](https://github.com/kizniche/Mycodo) is installable on e.g. a Raspberry Pi to aggregate and monitor sensor data and trigger functions
  * Hacker News [thread](https://news.ycombinator.com/item?id=26861471)
* [Home Assistant](https://www.home-assistant.io/) integrate all the things in your home automation setup.

### back up

* [Kopia](https://kopia.io/)
  * Hacker News [thread](https://news.ycombinator.com/item?id=37526477)
* [Optar](http://ronja.twibright.com/optar/) OPTical ARchiver for encoding data on paper
  * Hacker News [thread](https://news.ycombinator.com/item?id=30820836)
* [PaperBack](http://ollydbg.de/Paperbak/#1) encodes data on a single A4/Letter sheet 
  * Hacker News [thread](https://news.ycombinator.com/item?id=10245836) and [thread](https://news.ycombinator.com/item?id=29691021)
* [Duplicati](https://www.duplicati.com/) self-hostable encrypted backups for Windows, macOS and Linux

### blogging/publishing

* Write Freely (federated and self-hostable)
  * [Official instance](https://write.as/)
* [Wordpress](https://wordpress.com/)

### Bittorrent client

* [Transmission](https://en.wikipedia.org/wiki/Transmission_(BitTorrent_client))
  * Text user interface (TUI) for Transmission: [tormix](https://github.com/ckardaris/tormix)

### bookmark management

* [Espial](https://github.com/jonschoning/espial) is an open-source, web-based bookmarking server
  * Hacker News [thread](https://news.ycombinator.com/item?id=25316526)
* [floccus](https://floccus.org/) for syncing bookmarks to an arbitrary WebDAV server
* [Grimoire](https://github.com/goniszewski/grimoire)
  * Hacker News [thread](https://news.ycombinator.com/item?id=38419996)
* [Linkwarden](https://github.com/linkwarden/linkwarden) self-hostable collaborative bookmark manager
  * Hacker News [thread](https://news.ycombinator.com/item?id=36942308)
* [Shiori](https://github.com/go-shiori/shiori) "is a simple bookmarks manager written in the Go language. Intended as a simple clone of Pocket. You can use it as a command line application or as a web application. This application is distributed as a single binary, which means it can be installed and used easily."

### calendaring

* [Bloben](https://bloben.com/) self-hostable CalDAV client
  * Hacker News [thread](https://news.ycombinator.com/item?id=30929022)
* [cal.com](https://cal.com/) alternative to calendly

### cloud storage

E.g. Google Drive or Dropbox.

* [Cryptee](https://crypt.ee/)
* [cryptpad](https://cryptpad.fr/)
* [Nextcloud](https://nextcloud.com/) (supports integration with different cloud office suites)
  * [Collabora](https://www.collaboraoffice.com/) libre office in the cloud
  * [OnlyOffice](https://www.onlyoffice.com/) less featureful but uses MS office formats natively
* [Syncthing](https://syncthing.net/) simple file syncronisation between your systems

### chat (audio)

Primarily audio-based.

* [Baresip](https://github.com/baresip/baresip) is a "portable and modular SIP User-Agent with audio and video support"
  * Hacker News [thread](https://news.ycombinator.com/item?id=37367013)
* [Mornin](https://mornin.fm/) "is an instant audio conferencing service"
* [Mumble](https://www.mumble.info/) is a low latency, high quality voice chat tool
* [SonoBus](https://sonobus.net/) "is an easy to use application for streaming high-quality, low-latency peer-to-peer audio between devices over the internet or a local network"
  * Hacker News [thread](https://news.ycombinator.com/item?id=26148527)

### chat (text)

E.g. Slack, Discord.

The line can be blurry, but this section is for primarily text-based chat solutions.

* [Beeper](https://www.beeper.com/) consolidates chats from multiple apps and networks, including Matrix and proprietary ones, into one mobile app
* [Briar](https://briarproject.org/) secure messaging that can even operate without Internet
  * Hacker News [thread](https://news.ycombinator.com/item?id=29227754)
* [Chatcola](https://github.com/Chatcola-com/chatcola)
  * Note: It is technically not fully free software, but its code repository has a built-in time bomb mechanism that will apply the Apache 2.0 license in 2025.
* [Element](https://element.io/) (formerly Riot.im) does everything Basecamp, Slack, and Skype does and more.
  * Has Jitsi, Slack, and IRC integrations.
  * Uses the [Matrix protocol](https://en.wikipedia.org/wiki/Matrix_(protocol)).
    * Which can now be used for [online conferences](https://news.ycombinator.com/item?id=25630140) with 100s or 1000s of participants
    * Which Rocket.Chat [now ties into](https://matrix.org/blog/2022/05/30/welcoming-rocket-chat-to-matrix/)
* [Gitter](https://gitter.im/) is becoming open source after acquisition by Gitlab.
* [Jami](https://jami.net/)
* [Revolt](https://revolt.chat/)
  * Hacker News [thread](https://news.ycombinator.com/item?id=36433842)
* [Signal](https://whispersystems.org/)
* XMPP:
  * [trashserver.net](https://trashserver.net/en/), a 100% green energy XMPP server in Germany with fairly good privacy policy
    * [Hacker News thread](https://news.ycombinator.com/item?id=17068772)
  * Free Software Foundation membership comes with a XMPP handle
* [Zulip](https://zulipchat.com/), which is fully open source and claims to be the most actively developed.
  * [Hacker News thread](https://news.ycombinator.com/item?id=16863675)
  * Among other things, can *import* existing Slack workspaces.
* [Rocket Chat](https://www.rocket.chat/) has plans to interoperate with matrix protocol
* [SimpleX](https://simplex.chat/) e2ee no unique user identifiers, self-hostable relays

### chat (video conferencing)

E.g. Skype, Zoom.

Primarily video-based solutions.

* [Apache OpenMeetings](https://en.wikipedia.org/wiki/Apache_OpenMeetings) is a self-hostable web-conferencing solution with groupware features, including shared whiteboard, file-sharing, calendaring (for finding meeting times, etc.).
* [BigBlueButton](https://github.com/bigbluebutton/bigbluebutton) (BBB) "supports real-time sharing of audio, video, slides (with whiteboard controls), chat, and the screen. Instructors can engage remote students with polling, emojis, multi-user whiteboard, and breakout rooms."
  * [BBB@Scale](https://gitlab.com/bbbatscale/bbbatscale) from the Department of Computer Science at University of Applied Sciences Darmstadt "aims to be an all-in-one portal for managing tenants, users, rooms, recordings, and BBB servers in order to provide a solution for large-scale online learning scenarios."
  * [Hacker News thread](https://news.ycombinator.com/item?id=22591573)
  * [The Online Meeting Co-Operative](https://www.org.meet.coop/) provides a hosted instance of BigBlueButton, apparently enough to run conferences off of (suggested by @bhaugen from [GOSH forums](https://forum.openhardware.science/t/summary-of-gosh-community-call-one-week-to-add-your-questions-next-call-is-14-july-2020/2328/11))
  * [Faircam.net](https://faircam.net/) hosts an instance
  * [fairkom](https://www.fairkom.eu/en) hosts an instance of BBB as [fairteaching](https://www.fairkom.eu/en/fairteaching)
  * [CommunityBridge](https://communitybridge.com/) is another hoster that seems to host events on a case-by-case request basis and is funded via Open Collective
  * The Software Freedom Conservancy [offers BigBlueButton instance](https://sfconservancy.org/news/2023/aug/15/exit-zoom/) to open source projects on request
* [Dino](https://dino.im) uses Jabber/XMPP for text and audio/video conferencing
  * Hacker News [thread](https://news.ycombinator.com/item?id=30316755)
* [Hubl.in](https://github.com/linagora/hublin) (DEPRECATED)
* [LiveKit](https://livekit.io/) is a software library for building live audio or video conferencing into other applications
  * So, technically this is not a video chat application in itself, but hopefully other solutions will come out of it.
* [MiroTalk](https://sfu.mirotalk.org)
  * Reddit [thread](https://old.reddit.com/r/selfhosted/comments/tnh3gs/webrtcp2psfu_open_source_alternative_to_zoom/)
* [Mozilla Hubs](https://hubs.mozilla.com/) community gathering virtual space
  * Hacker News [thread](https://news.ycombinator.com/item?id=30515097)
* [Jitsi Meet](https://meet.jit.si/)
  * [darmstadt.social instance](https://meet.darmstadt.social)
  * [Disroot instance](https://calls.disroot.org/)
  * [Element instance](https://jitsi.riot.im/)
  * [fairkom instance](https://fairmeeting.net/)
  * [IHE Delft instsance](https://jitsi-meet-ocw.un-ihe.org/)
  * [Open Solution instance](https://webmeeting.opensolution.it)
  * [Calla](https://github.com/capnmidnight/Calla) adds audio spatialization and virtual meeting rooms
    * Hacker News [thread](https://news.ycombinator.com/item?id=24710948)
* [Pyrite](https://github.com/garage44/pyrite)
  * Hacker News [thread](https://news.ycombinator.com/item?id=30398192)
* [Screego](https://screego.net/) is specifically for screen-sharing via WebRTC.
  * [Reddit thread](https://old.reddit.com/r/selfhosted/comments/j60eng/screego_multi_user_screen_sharing_via_browser/)
  * [Demo/public instance](https://app.screego.net/)
* [tlk](https://github.com/vasanthv/tlk) is a browser based WebRTC peer-to-peer (P2P) video call service that is self-hostable
  * Hacker News [thread](https://news.ycombinator.com/item?id=34107240)
* [webcam-filters](https://github.com/jashandeep-sohi/webcam-filters) "Add filters (background blur, etc) to your webcam on Linux"
  * Hacker News [thread](https://news.ycombinator.com/item?id=28362199)

### collaborative drawing

E.g. [Miro](https://miro.com/).

* [Drawpile](https://drawpile.net/)
  * Hacker News [thread](https://news.ycombinator.com/item?id=28211891)
* [Excalidraw](https://github.com/excalidraw/excalidraw) is also a real-time collaborative whiteboard, but, at time of writing, also has an export function
  * [Hacker News thread](https://news.ycombinator.com/item?id=23525648)
  * [excalidraw-claymate](https://github.com/dai-shi/excalidraw-claymate) is a tool for creating stop motion animations
* [No Man's Canvas](https://github.com/vkoskiv/nmc2) pixel drawing board
  * [Frontend](https://github.com/EliasHaaralahti/No-Mans-Canvas-Client)
  * [Older version](https://github.com/vkoskiv/NoMansCanvas)
  * Hacker News [thread](https://news.ycombinator.com/item?id=30167391)
* [Ourboard](https://github.com/raimohanska/ourboard) online whiteboard
  * Hacker News [thread](https://news.ycombinator.com/item?id=28808556)
* [Spacedeck](https://spacedeck.com/) also a collaborative board/canvas (GitHub [repository](https://github.com/spacedeck/spacedeck-open)) that's now being developed by MNT, the group behind the fully open source hardware and free software [MNT Reform](https://www.crowdsupply.com/mnt/reform) computer
* [TheBoard](https://github.com/toger5/TheBoard) is an under-development collaborative whiteboard that is powered by the Matrix protocol and infrastructure
  * Hacker News [thread](https://news.ycombinator.com/item?id=28017327)
* [Whitebird](https://github.com/BuchholzTim/Whitebird) "is an open-source, web-based, collaborative, digital whiteboard"
  * [Reddit announcement](https://old.reddit.com/r/selfhosted/comments/lnddc7/whitebird_a_opensource_webbased_collaborative/)
* [whitebophir](https://github.com/lovasoa/whitebophir) is an online real-time collaborative whiteboard
  * [Hacker News thread](https://news.ycombinator.com/item?id=18551874) (wb)
### commenting platform

E.g. Disqus.

* [Long list](https://lisakov.com/projects/open-source-comments/) of "open-source self-hosted comments for a static website".
  * [Hacker News thread](https://news.ycombinator.com/item?id=24676152)
* [Cactus Comments](https://cactus.chat/) is a federated comment system for the web, based on the Matrix protocol: https://cactus.chat/
  * Hacker News [thread](https://news.ycombinator.com/item?id=26371813)
* [Commento](https://commento.io/)
  * [Hacker News thread](https://news.ycombinator.com/item?id=19210697) (wb)
  * [Another Hacker News thread](https://news.ycombinator.com/item?id=19439433) (wb)
* [Coral](https://github.com/coralproject/talk) is developed by Vox Media and looks more comprehensive then the light-weight solutions
* [Cusdis](https://github.com/djyde/cusdis) is light-weight and self-hosted, requiring moderator approval for every post
  * Hacker News [thread](https://news.ycombinator.com/item?id=26878153)
* [Remark42](https://github.com/umputun/remark42) is a self-hosted, lightweight, and simple (yet functional) comment engine
* [Talk](https://coralproject.net/talk/)
  * [Hacker News thread](https://news.ycombinator.com/item?id=20474878) (wv)
* [hypothesis](https://web.hypothes.is/) annotation style comments

### customer relationship management (CRM)

E.g. Salesforce

* [Crust CRM suite](https://www.crust.tech/crust-releases-version-2020-06-and-removes-paywall-for-all-products/)
  * [Phoronix report](https://www.phoronix.com/scan.php?page=news_item&px=Crust-Goes-No-Paywall)
* [Monica](https://github.com/monicahq/monica) is a personal CRM for friends, family, or professional contacts
  * Hacker News [thread](https://news.ycombinator.com/item?id=25270001)
* [CiviCRM](https://civicrm.org/) popular with large non-profits
* [Odoo](https://www.odoo.com/) small and medium sized business focused

### databases

* [Baserow.io](https://baserow.io/) is a self-hostable replacement for Airtables
  * Hacker News [thread](https://news.ycombinator.com/item?id=26448985)
  * Reddit [thread](https://old.reddit.com/r/selfhosted/comments/mmv90c/releasing_baserow_11_with_search_reordering_of/)
* [data-diff](https://github.com/datafold/data-diff) compare "tables within or across databases"
  * Hacker News [thread](https://news.ycombinator.com/item?id=31837307)
* [NocoDB](https://github.com/nocodb/nocodb) is another self-hostable Airtables replacement that "turns any MySQL, PostgreSQL, SQL Server, SQLite & MariaDB into a smart-spreadsheet" released under the AGPLv3
  * Hacker News [thread](https://news.ycombinator.com/item?id=27303783)
* [Skytable](https://github.com/skytable/skytable) "is a fast, secure and reliable realtime NoSQL database with keyspaces, tables, data types, authn/authz, snapshots"
  * [Blog post](https://blog.skytable.io/understanding-the-agpl) explaining the implications of its AGPLv3 license.
  * Reddit [thread](https://old.reddit.com/r/opensource/comments/uaoeiw/understanding_the_agpl_the_most_misunderstood/)

### data repositories

* [Open Science Framework](https://osf.io/)
  * Create projects" in which all of your project files reside with DOI provided, can pull and integrate files from Dropbox, GitHub, Google Drive, Mendeley, etc.
* [Zenodo](https://zenodo.org/)

### data visualisation

There are so many options, here are just a few that piqued my interest.

* [Matplotlib](https://en.wikipedia.org/wiki/Matplotlib) is the classic Python-based plotting library.
* [Veusz](https://veusz.github.io/) is primarily a classic GUI-based scientific plotting program for Windows, Mac OS, and GNU/Linux.
  * But it's also completely programmable from Python 3, so it's almost like [Matplotlib](https://en.wikipedia.org/wiki/Matplotlib) but with a full-featured GUI.
  * I'm impressed that a classic style plotting program is still maintained in 2020.

### desktop publishing

E.g. Adobe InDesign.

* [Scribus](http://www.scribus.net/)

### digital asset management (DAM)

* [Pimcore](https://pimcore.com/en/products/data-manager/digital-asset-management/introduction)
* [Razuna](http://www.razuna.org/whatisrazuna)
* [Fathom](https://usefathom.com/) also looks good

### discussion forums

* [Discourse](https://www.discourse.org/)
  * It can also be used to host mailing lists

### ebooks

* [Calibre](https://calibre-ebook.com/) is a complete ebook management suite with library management, ebook-editing, and sync to readers, etc.
* [Sigil](https://github.com/Sigil-Ebook/Sigil) is a multi-platform EPUB ebook editor
  * Hacker News [thread](https://news.ycombinator.com/item?id=24140752)

### educational

* [ClassQuiz](https://github.com/mawoka-myblock/classquiz) is an open source and self-hostable Kahoot clone for use in classroom settings
  * Reddit [thread](https://old.reddit.com/r/opensource/comments/tb40ch/opensource_kahootclone/)
* [Edrys](https://github.com/edrys-org/edrys) is an open-source remote teaching platform
  * Hacker News [thread](https://news.ycombinator.com/item?id=30728064)
* [Manim](https://github.com/ManimCommunity/manim) animation engine for explanatory math videos
  * Hacker News [thread](https://news.ycombinator.com/item?id=31636657)
#### electronics

* [Digital](https://github.com/hneemann/Digital) is an easy-to-use digital logic designer and circuit simulator designed for educational purposes
  * Hacker News [thread](https://news.ycombinator.com/item?id=26613641)

#### language

* [02books](https://02books.app/) (GitHub [repository](https://github.com/sofignatova/02books)) teaches reading to kids
  * Hacker News [thread](https://news.ycombinator.com/item?id=25501294)

#### physics

* [ThePhysicsHub](https://github.com/ThePhysHub/ThePhysicsHub) basic physics simulations mostly in Javascript.
  * Reddit [thread](https://old.reddit.com/r/github/comments/i5bw6l/we_have_created_an_open_source_physics/)

### email

* [Betterbird](https://www.betterbird.eu/) is a fine-tuned version of Thunderbird
* [Thunderbird](https://www.thunderbird.net/)

### encryption

* [Age](https://github.com/FiloSottile/age/) "is a simple, modern and secure file encryption tool, format, and Go library"
  * Hacker News [thread](https://news.ycombinator.com/item?id=28435613)
* [Dark Crystal](https://darkcrystal.pw/) is a set of protocols, libraries, techniques and guidelines for secure management of sensitive data such as cryptographic keys
  * Hacker News [thread](https://news.ycombinator.com/item?id=29800305)
* [Horcrux](https://github.com/jesseduffield/horcrux) "[splits] your file into encrypted horcruxes so that you don't need to remember a passcode"
* [Picocrypt](https://github.com/HACKERALERT/Picocrypt) is a tiny, secure, and simple to use file encryption tool
  * "Picocrypt is very secure. It uses XChaCha20-Poly1305 as the cipher and MAC, as well as Argon2(id) for key derivation. SHA3-512 is also used for key checking and file corruption checks. It's reliable and actively prevents file corruption by using Reed-Solomon parity bytes, which can recover corrupted bytes. Picocrypt using the Pycryptodome and Argon2-cffi Python libraries, which are well known in the Python community."
  * "It comes with a simple GUI, and it's very lightweight."
  * Aims to be simpler version of VeraCrypt while being secure
  * Keyfile support coming
  * Reddit accouncement [thread](https://old.reddit.com/r/privacytoolsIO/comments/m8jpu6/picocrypt_a_foss_3mb_encryption_tool/)
* [SiriKali](https://mhogomchungu.github.io/sirikali/) is a GUI frontend to create and manage encrypted folders with tools like  ecryptfs, cryfs, encfs, gocryptfs, fscrypt and securefs
### events management

E.g. Eventbrite, Facebook Events, Meetup.

* Eventbrite replacement:
  * [eventyay](https://eventyay.com/)
* [Mobilizon](https://mobilizon.org/) is a Activitypub federated events and groups manager developed by Framasoft.
  * [1.0 announcement thread/post](https://news.ycombinator.com/item?id=24906218)
* Reddit thread on "[Facebook Event alternative](https://redd.it/8c46wf)"

### file-sending

E.g. WeTransfer.

* [croc](https://github.com/schollz/croc) is similar to [magic-wormhole](https://github.com/warner/magic-wormhole) but makes transferring multiple files easier and has end-to-end encryption, among other features.
* [FilePizza](https://file.pizza/) (via WebRTC so both sender and receiver has to be online at same time) (seems a bit unstable at the moment)
* Lufi instances, which are end-to-end (E2E) encrypted:
  * [Framadrop](https://framadrop.org/)
  * [disroot](https://upload.disroot.org/)
* [Firefox Send](https://send.firefox.com/) ([E2EE](https://en.wikipedia.org/wiki/End-to-end_encryption) emphemeral links)
  * Mozilla is not longer actively developing Firefox Send but fortunately others have picked up the slack such as a fork simply called [Send](https://github.com/timvisee/send)
    * [List](https://github.com/timvisee/send-instances/) of Send instances
    * Hacker News [thread](https://news.ycombinator.com/item?id=27052840) discussing similar solutions
* [Gokapi](https://github.com/Forceu/Gokapi) allows authorised users to upload and share files
* [Null Pointer](https://0x0.st/) provides file-hosting/sending and URL shortening services and is self-hostable
  * Uploading files, however, requires using the HTTP POST request
* [OnionShare](https://onionshare.org/) is a file-sending tool that uses the Tor network
  * [redbean-onionshare] is an implementation of OnionShare but as a standalone, multi-platform [Actually Portable Executable](https://justine.lol/ape.html)
    * Hacker News [thread](https://news.ycombinator.com/item?id=28051222)
* [OpenDrop](https://github.com/seemoo-lab/opendrop) is an implementation of Apple's AirDrop that aims to be compatible with Apple devices
  * Hacker News [thread](https://news.ycombinator.com/item?id=29768376)
* [Plik](https://github.com/root-gg/plik) is "a temporary file upload system (Wetransfer like) in Go"
* [Pomf](https://github.com/pomf/pomf) is a "simple file uploading and sharing platform"
  * There are many hosted public instances of Pomf and its derivatives, listed [here](https://pomfcrawl.pythonanywhere.com/) and [here](https://status.uguu.se/clones.html)
* [Snapdrop](https://github.com/RobinLinus/snapdrop) uses WebRTC and is for file-transfers between clients on the same network, similar to Apple's Airdrop
  * [Hosted instance](https://snapdrop.net/)
* [transfer.sh](https://transfer.sh/) command line based sharing
* [WebWormHole](https://webwormhole.io/) file-sending also uses WebRTC to establish peer-to-peer connections
  * Hacker News [thread](https://news.ycombinator.com/item?id=23023675)
  * [Git repository](https://github.com/saljam/webwormhole)
* [wormhole-gui](https://github.com/Jacalz/wormhole-gui) is a graphical interface for the [Go implementation](https://github.com/psanford/wormhole-william) of [magic-wormhole](https://github.com/magic-wormhole/magic-wormhole) that allows you to share files between devices on the same local area network
  * Hacker News [thread](https://news.ycombinator.com/item?id=26465854)

### fonts

E.g. Google Fonts.

* [Open Font Library](https://openfontlibrary.org/)
  * Has an API similar to Google Fonts, but without Google.
* [League of Moveable Type](https://www.theleagueofmoveabletype.com/)

### graphics

* [epick](https://github.com/vv9k/epick) is a "color picker for creating harmonic color palettes that works on Linux, Windows, macOS and web"
* [Graphite](https://graphite.rs/) Open-source raster and vector 2D graphics editor
  * Hacker News [thread](https://news.ycombinator.com/item?id=36901406)
* [hue.tools](https://github.com/pabueco/hue.tools) is a "toolbox for working with colors. Color mixing, blending, conversion, modification, detailed information, etc." and just generally for coming up with useful colors
  * [Live version](https://hue.tools/)
  * Hacker News [thread](https://news.ycombinator.com/item?id=30362112)
* [Screenshot.rocks](https://github.com/daveearley/screenshot.rocks) is an online tool which allows you to create beautiful mobile & browser mockups from screenshots
  * Hacker News [thread](https://news.ycombinator.com/item?id=34790781)

#### diagrams

* [Draw.io](https://app.diagrams.net/) (now [diagrams.net](https://www.diagrams.net/))
  * [extension for Visual Studio Code](https://news.ycombinator.com/item?id=23138507) that can embed Draw.io diagrams *inside* PNG images! (is this a function of the extension or draw.io itself?)
  * Creating [interactive diagrams](https://www.drawio.com/blog/interactive-diagram-layers) with Draw.io
* [Graphviz](https://graphviz.org/)
  * Extensions for Visual Studio Code [here](https://marketplace.visualstudio.com/items?itemName=tintinweb.graphviz-interactive-preview) and [here](https://marketplace.visualstudio.com/items?itemName=joaompinto.vscode-graphviz)
* [Pencil](https://pencil.evolus.vn/) is for mockups and prototyping
  * Hacker News [thread](https://news.ycombinator.com/item?id=27048163)
* [Penpot](https://penpot.app/) is a design and prototyping platform based on SVGs
  * Hacker News [thread](https://news.ycombinator.com/item?id=30407913)
* [Penrose](https://penrose.cs.cmu.edu/) is a tool for "making beautiful diagrams in any area of science, mathematics, or engineering... just by typing notation in plain text"
  * Hacker News [thread](https://news.ycombinator.com/item?id=36746047)
* [Pointless](https://github.com/kkoomen/pointless) is an "endless drawing canvas that provides useful features when you're in need for a simple whiteboard/note app"
  * Hacker News [thread](https://news.ycombinator.com/item?id=31308312)
* [ThinkComposer](http://www.thinkcomposer.com/) for making advanced concept maps, mind maps flowcharts, models and diagrams ([GPLv3](http://www.thinkcomposer.com/Home/ProductFaq.html))
  * Hacker News [thread](https://news.ycombinator.com/item?id=30669754)
* [tldraw](https://github.com/tldraw/tldraw) is an infinite canvas collaborative whiteboard
  * Hacker News [thread](https://news.ycombinator.com/item?id=33818822)
* [Wireflow](https://github.com/vanila-io/wireflow) user experience flow chart and collaboration tool
  * Hacker News [thread](https://news.ycombinator.com/item?id=24624410)

#### raster

E.g. Adobe Photoshop.

* [AzPainter](https://github.com/Symbian9/azpainter)
* [Beaver's Image Editing Software](https://github.com/stars/LinuxBeaver/lists/beaver-s-image-editing-software) is a list of tools supporting GIMP and Pixelitor such as artificial intelligence to help with processing
  * Reddit [thread](https://old.reddit.com/r/opensource/comments/u8r5mq/i_want_to_popularize_open_source_machine_learning/)
* [GIMP](http://www.gimp.org/)
* [jskidpix](https://kidpix.app/) is a web-based re-implementation of Kid Pix, the drawing program for children which was dedicated to the public domain
  * GitHub [repository](https://github.com/vikrum/kidpix)
  * Hacker News [thread](https://news.ycombinator.com/item?id=28073383)
* [Open Brush](https://github.com/icosa-gallery/open-brush/) is "a room-scale 3D-painting virtual-reality application"
* [Photoflare](https://photoflare.io/)
* [Pixelitor](https://pixelitor.sourceforge.io/)
* [stitching](https://github.com/lukasalexanderweber/stitching) is a Python package for fast and robust image stitching using OpenCN's stitching module
  * Hacker News [thread](https://news.ycombinator.com/item?id=32466003)
* [Upscayl](https://github.com/TGS963/upscayl) is an AI image upscaler
* [Krita](https://krita.org/en/) professional open source painting program

#### vector

E.g. Adobe Illustrator.

* [Dotgrid](https://100r.co/site/dotgrid.html) is a grid-based vector drawing software designed to create logos, icons and type
  * Supports layers, the full SVG specs
* [Inkscape](https://inkscape.org/)
* [Lorien](https://github.com/mbrlabs/Lorien) is an infinite canvas drawing/note-taking app that is focused on performance, small savefiles and simplicity
  * Hacker News [thread](https://news.ycombinator.com/item?id=31717007) with other suggestions
* [Minimator](https://minimator.app) is a minimalist graphical editor for line-based illustration on a grid
  * Code [on GitHub](https://github.com/maxwellito/minimator/)
  * Hacker News [thread](https://news.ycombinator.com/item?id=29838197)
* [Motion Canvas](https://github.com/motion-canvas/motion-canvas) is a tool to programmatically define animations via the web browser Canvas API via Typescript
  * Hacker News [thread](https://news.ycombinator.com/item?id=34897707)
* [SvgPathEditor](https://github.com/Yqnn/svg-path-editor) allows you to "edit or create SVG paths in browser" for simple vector graphics
  * Hacker News [thread](https://news.ycombinator.com/item?id=30382577)
* [VGC](https://www.vgc.io/)
* [Wick](https://www.wickeditor.com/) 2D interactive animations editor
  * [Workshop](https://schedule.mozillafestival.org/session/QEMJCM-1) at MozFest 2021
* [Xfig](http://mcj.sourceforge.net/)

### group collaboration platform

E.g. Basecamp, Microsoft Teams.

* [Agorakit](https://www.agorakit.org/) for "creating collaborative groups, [so] people can discuss topics, organize events, store files and keep everyone updated"
* [Loomio](https://en.wikipedia.org/wiki/Loomio) allows group collaboration including discussions, issues, and polls to aid decision making
  * [Framavox](https://framavox.org/) is an instance operated by Framasoft
* [Mobilizon](https://mobilizon.org/about) is "user-friendly, emancipatory and ethical tool for gathering, organising, and mobilising"
* [Retroshare](https://retroshare.cc/) is a P2P decentralised network/groupware with chat, mail, file-sharing, forums, links, channels, and voice/video calls.
  * [Hacker News thread](https://news.ycombinator.com/item?id=22698306) (wb)
* [Twake](https://github.com/TwakeApp/Twake) is a new but rapidly-developed collaboration platform aimed at replacing Microsoft Teams.

### integrated development environment (IDE)

* [CardStock](https://github.com/benjie-git/CardStock) "is a cross-platform tool for quickly and easily building graphical programs, called stacks, which can be made up of multiple pages called cards. It provides a drawing-program-like editor for building Graphical User Interfaces, and a code editor for adding event-driven python code."
  * Hacker News [thread](https://news.ycombinator.com/item?id=35708580)
* [Decker](https://github.com/JohnEarnest/Decker) HyperCard clone.
  * Hacker News [thread](https://news.ycombinator.com/item?id=33377964)
* [NoteCalc](https://bbodi.github.io/notecalc3/) "is a handy notepad with a smart builtin calculator"
  * Hacker News [thread](https://news.ycombinator.com/item?id=25495393) with links to similar tools

### internet reclamation projects

As defined by Drew DeVault in [this post](https://drewdevault.com/2021/09/23/Nitter-and-other-internet-reclamation-projects.html) and as discussed [here](https://news.ycombinator.com/item?id=28660261). This section mostly contains free software proxies to or downloaders for proprietary platforms. Relevant lists: 

* [This GitHub repository](https://github.com/mendel5/alternative-front-ends)
  * Hacker News [thread](https://news.ycombinator.com/item?id=29620275)
* [Or this list](https://github.com/digitalblossom/alternative-frontends)
  * Hacker News [thread](https://news.ycombinator.com/item?id=29662235)
* [Or this other list](https://github.com/mendel5/alternative-front-ends)
* [This overview](https://drewdevault.com/2021/09/23/Nitter-and-other-internet-reclamation-projects.html)

#### hosted

* [Farside](https://github.com/benbusby/farside) provides links that automatically redirect to working instances of privacy-oriented alternative frontends, such as Nitter, Libreddit, etc.
  * Reddit [thread](https://old.reddit.com/r/thehatedone/comments/thtvzg/farside_a_smart_redirecting_gateway_for_various/)
  * [Sourcehut repository](https://sr.ht/~benbusby/farside/)
* [Bibliogram](https://bibliogram.art/) for Instagram
  * Sadly no longer in development, because the Instragram API that it talks to does too much rate limiting. But there are some non-open source proxies [here](https://github.com/zedeus/nitter/issues/761#issuecomment-1400251931).
* [Bird.makeup](https://bird.makeup/) is a Twitter to ActivityPub bridge
  * Hacker News [thread](https://news.ycombinator.com/item?id=34748669) with links to similar tools
* Google Translate proxies
  * [gtranslate](https://gtranslate.metalune.xyz/)
    * [Source code](https://git.sr.ht/~yerinalexey/gtranslate)
  * [Lingva Translate](https://lingva.ml/)
    * [Source code](https://github.com/TheDavidDelta/lingva-translate)
  * [SimplyTranslate](https://translate.metalune.xyz/)
    * Also acts as frontend to [Libretranslate](#translation)
    * Reddit [thread](https://old.reddit.com/r/privacytoolsIO/comments/n1u9oa/privacyoriented_frontend_for_google_translate/)
    * [Source code](https://git.sr.ht/~metalune/simplytranslate_web)
* [Imgin](https://git.voidnet.tech/kev/imgin) is a minimalist read-only Imgur proxy
* [Invidious](https://github.com/iv-org/invidious) for YouTube
* [LibRedirect](https://libredirect.github.io) is a "web extension that redirects YouTube, Twitter, Instagram... requests to [...] privacy friendly frontends and backends"
  * Hacker News [thread](https://news.ycombinator.com/item?id=32288732)
* [Nitter](https://nitter.net) for Twitter
* [Parasitter](https://github.com/pluja/Parasitter) for YouTube
* [Privacy Redirect](https://github.com/SimonBrazell/privacy-redirect) is another "web extension that redirects Twitter, YouTube, Instagram, Google Maps, Reddit, Google Search, & Google Translate requests to privacy friendly alternative frontends for those sites", similar to LibRedirect
  * Hacker News [thread](https://news.ycombinator.com/item?id=28903419)
* [Spacebar](https://github.com/spacebarchat/spacebarchat) is a "free, opensource selfhostable discord-compatible chat, voice and video platform"
* [Scribe](https://scribe.rip) for Medium
* [Teddit](https://teddit.net/) for Reddit
  * Hacker News [thread](https://news.ycombinator.com/item?id=25310206)
* [Yotter](https://github.com/ytorg/Yotter) for Twitter *and* YouTube

#### local

I.e. install on your own device.

* [Freetube](https://freetubeapp.io/) for YouTube
* [gallery-dl](https://github.com/mikf/gallery-dl) for downloading from image hosting platforms like Imgur or Deviantart
  * Hacker News [thread](https://news.ycombinator.com/item?id=25244589)
* [InstaGrabber](https://github.com/austinhuang0131/instagrabber) for Instagram
* [NewPipe](https://newpipe.schabi.org/) for YouTube on Android devices
* [Tartube](https://tartube.sourceforge.io/) for YouTube, which uses...
* [youtube-dl](https://ytdl-org.github.io/youtube-dl/) is the definitive video downloader

### internet speed test

E.g. [speedtest.net](https://www.speedtest.net/)

* [LibreSpeed](https://librespeed.org/)
  * [Reddit thread](https://redd.it/fstpke) which discusses this service and suggests other (possibly non-free, but not speedtest.net) tests
* [OpenSpeedTest](https://github.com/openspeedtest/Speed-Test)
  * Reddit [thread](https://old.reddit.com/r/freesoftware/comments/zev5l6/free_and_opensource_selfhosted_html5_speedtest/)

### lab notebooks

* [eLabFTW](https://www.elabftw.net/)
* [Zettlr](https://www.zettlr.com/) Markdown notebook with Zotero and other reference manager support!

### library management

For managing physical libraries.

* [Greenstone](https://www.greenstone.org/) is a "suite of software for building and distributing digital library collections"
  * Mentioned in this [thread](https://floss.social/@mikeday@ohai.social/110841473766759211)
* [Koha](https://koha-community.org/)
  * Mentioned in [this Reddit thread about setting up a library](https://old.reddit.com/r/solarpunk/comments/jrpfzb/libraries_for_all_how_to_start_and_run_a_basic/).

### link aggregator

E.g. Reddit.

* [Lemmy](https://github.com/LemmyNet/lemmy) is a live-updating, federated (soon to have ActivityPub support) Reddit replacement that is self-hostable.
  * [Reddit thread](https://old.reddit.com/r/linux/comments/guklhr/we_are_the_devs_behind_lemmy_an_open_source/)

### mailing lists and marketing

E.g. Mailchimp.

* [Mautic](https://www.mautic.org/)
* [Mailtrain](https://www.reddit.com/r/selfhosted/duplicates/8d46k8/mailtrainorg_selfhosted_opensource_newsletter_app/)
  * [Hacker News thread](https://news.ycombinator.com/item?id=16862545)

### maps

E.g. Google Maps.

* [Headway](https://github.com/ellenhp/headway) is a "self-hostable maps stack, powered by OpenStreetMap"
  * Hacker News [thread](https://news.ycombinator.com/item?id=31536217)
* [kvtiles](https://blog.nobugware.com/post/2020/free-maps-for-all/) self-hosted Docker container "for a self hosted free performant world map."
* [OpenStreetMap](https://www.openstreetmap.org/)

* [OSMand](https://osmand.net/) Mobile (android & iOS) open street map client, offline maps, turn-by-turn navigation and highly customisable

### news/feed/RSS reader

* [newspipe](https://sr.ht/~cedric/newspipe/) "is a web news aggregator"

### notes management

E.g. Evernote, Microsoft OneNote.

* [Turtl](https://turtlapp.com/)
* [Laverna](https://laverna.cc/)
* offline (Mac-only...) SubEthaEdit 5 is [now open source](https://news.ycombinator.com/item?id=18550649) (wb)
* [Joplin](https://joplinapp.org/)
* [nvpy](https://github.com/cpbotha/nvpy) is a Python clone of Notational Velocy for quick note-taking and search
* [QOwnNotes](https://www.qownnotes.org/)
* [Logseq](https://logseq.com/) a sequence of logs - powerful and feature rich bidirecational linking based notes organisation
* [trilium](https://github.com/zadam/trilium/wiki) self-hostable note taking app, highly extensible

### office suites

E.g. Microsoft Office.

* [LibreOffice](https://www.libreoffice.org/)

### pastebins text/data-sharing

* [Burner Note](https://github.com/GigaMick/burnernote) is a free, ad-free and open source tool for securely sending text based notes that are encrypted and self destruct once read
  * Reddit [thread](https://old.reddit.com/r/opensource/comments/r93f1w/free_adfree_and_open_sourced_service_for_sending/)
  * [Official instance](https://burnernote.com/)
* [Drift](https://github.com/maxleiter/drift) is a self-hostable text pastebin and GitHub Gist replacement
  * Reddit [thread](https://old.reddit.com/r/selfhosted/comments/tpqpvk/wrote_up_an_introduction_to_drift_a_selfhosted/)
* [MicroBin](https://github.com/szabodanika/microbin) comes with "URL redirection, automatic file expiry, raw file serving, 5-level privacy setting. QR code sharing and a bunch more"
  * Reddit [thread](https://old.reddit.com/r/selfhosted/comments/14x36vs/microbin_v2_released/)
* [PrivateBin](https://privatebin.info/) instances:
  * [CHATONS](https://paste.nomagic.uk/)
* [pubKey](https://www.pubkey.pm/) "is a proof-of-concept of a decentralised, multi-key-encryption, zero knowledge cryptographic protocol. Additionally, this page uses **NO SERVER**, stores **NO DATA** and is completely **CLIENT SIDE**."
  * Hacker News [thread](https://news.ycombinator.com/item?id=32136202)
* [Textdb](https://textdb.dev/) is a "simple way to share small amounts of data" via HTTP requests

### PDF tools

* [Densify](https://github.com/hkdb/Densify) is a GTK- and Ghostscript-based GUI tool for compressing PDF files
* [diff-pdf](https://github.com/vslavik/diff-pdf) is a "tool for visually comparing two PDFs"
* [Documenso](https://github.com/documenso/documenso) self-hostable document signer to replace Docusign
* [Docuseal](https://github.com/docusealco/docuseal) self-hostable document signer to replace Docusign
  * Hacker News [thread](https://news.ycombinator.com/item?id=36798593)
  * Reddit [thread](https://old.reddit.com/r/opensource/comments/153ue2d/open_source_docusign_alternative/)
* [Okular](https://okular.kde.org) has relatively good annotation support
* [OpenSign](https://github.com/OpenSignLabs/OpenSign) self-hostable document/PDF signer to replace Docusign
  * Hacker News [thread](https://news.ycombinator.com/item?id=38052344)
* [peepdf](https://github.com/jesparza/peepdf) is a "Python tool to explore PDF files in order to find out if the file can be harmful or not"
  * Could be useful for e.g. checking for security risks or scrubbing metadata like a fingerprint Elsevier might embed in a paper's PDF to track you
  * Hacker News [thread](https://news.ycombinator.com/item?id=36746110)
* [Sioyek](https://github.com/ahrm/sioyek) is a PDF viewer designed for reading research papers and technical books

### personal finance

* GNUCash
* Firefly-III
* ledger

### photo gallery

* [LibrePhotos](https://github.com/LibrePhotos/librephotos) (sh)
  * Hacker News [thread](https://news.ycombinator.com/item?id=25588712)
  * Reddit [thread](https://old.reddit.com/r/opensource/comments/s3x2z4/librephotos_a_selfhosted_open_source_photo/)
* [PhotoPrism](https://github.com/photoprism/photoprism) (sh)
* [immich](https://immich.app/) self-hostable with offline AI image sorting

### podcasts

#### hosting/publishing

* [Podcast Generator](https://podcastgenerator.net/)
* [Podlove](https://github.com/podlove) podcasting software & standards
* Reddit [discussion](https://old.reddit.com/r/selfhosted/comments/kain8w/how_do_you_self_host_your_podcast/) on solutions

### polling/voting

E.g. Doodle polls.

* [Dudle](https://dudle.inf.tu-dresden.de/)
* [Framadate](https://framadate.org/)
  * CHATONS lists [some Framadate instances](https://entraide.chatons.org/en/)
  * There is also [an instance](https://terminplaner4.dfn.de/) hosted by the German National Research and Education Network (DFN)
* [Rallly](https://github.com/lukevella/rallly)
  * Reddit [thread](https://old.reddit.com/r/selfhosted/comments/u98s4t/i_built_an_opensource_doodle_poll_alternative/)
* [SnoopForms](https://snoopforms.com/)
  * Hacker News [thread](https://news.ycombinator.com/item?id=32303986)
* [STAR voting](https://star.vote/) which implements the score, then automatic runoff (STAR) method.
  * [git repository](https://github.com/msmunter/star.vote)
* [Cornell Condorcet Internet Voting Service](https://civs.cs.cornell.edu/) which allows condorcet ranked-choice voting.

### point of sales

* [Shopya](https://github.com/Abdur-rahmaanJ/shopyo) point of sales and inventory management software
  * Particular emphasise on welcoming and guiding new contributors

### presentations

E.g. Microsoft Powerpoint.

* [Decker](https://github.com/JohnEarnest/Decker) is "a multimedia platform for creating and sharing interactive documents, with sound, images, hypertext, and scripted behavior" based on Hypercard
  * Hacker News [thread](https://news.ycombinator.com/item?id=33478246)
* [LibreOffice Impress](https://www.libreoffice.org/)
* [opensource.com article about slide generators](https://opensource.com/article/18/5/markdown-slide-generators)
  * Good for version control, but far from enough flexibility. Presentations are very visual, and it's hard to code them in Markdown.
* [Sozi](https://sozi.baierouge.fr/) is a "zooming" presentation maker using SVG files
* [Strut](https://github.com/tantaman/Strut) is and Impress.js and Bespoke.js presentation editor

#### keystroke visualiser

When sharing your screen to demonstrate things during a presentation, sometimes it helps to have a keystroke visualiser that shows on-screen which keys you are pressing.

* [KeyCastr](https://github.com/keycastr/keycastr) for macOS
* [Showkeys](https://github.com/nibrahim/showkeys) for GNU/Linux (X11)
* [ScreenKey](https://www.thregr.org/wavexx/software/screenkey/) for GNU/Linux (X11)
* [showmethekey](https://showmethekey.alynx.one/) for GNU/Linux (X11 or Wayland)
* For Microsoft Windows: 
  * [Carnac](https://github.com/Code52/carnac)
  * [Kling](https://github.com/KaustubhPatange/Kling)
  * [YetAnotherKeyDisplayer](https://github.com/Jagailo/YetAnotherKeyDisplayer)

### project management

* [Leantime](https://github.com/Leantime/leantime) "is a lean open source project management system for startups and innovators"
* [OpenProject](https://www.openproject.org/) supports classic, agile or hybrid project management
  * Hacker News [thread](https://news.ycombinator.com/item?id=24816065) with other suggestions
* [ProjeQtoR](https://www.projeqtor.org/)
* [Projectlibre](https://www.projectlibre.com/)
  * Designed to replace Microsoft Project
* [TaskJuggler](https://taskjuggler.org/)
  * Looks very versatile from managing simple to complex projects
  * Notable for its directly editable, pure-text file format that can be version-controlled
  * Hacker News [thread](https://news.ycombinator.com/item?id=28706503)
* [Taiga](https://taiga.io/) hosted or self-hostable Agile project management solution

### reference/bibliography management

E.g. Mendeley, RefWorks, or EndNote.

* [Docear](https://www.docear.org/) (amazing feature set but seemingly unmaintained...)
* [JabRef](https://www.jabref.org/)
* [Zotero](https://www.zotero.org/)

### remote access

E.g. TeamViewer, Windows Remote Desktop.

Don't forget [SSH](https://en.wikipedia.org/wiki/Secure_Shell) is almost always available and can tunnel other traffic.

* [DWS remote control](https://www.dwservice.net/) (need to double check if entire software stack if free software)
* [Guacamole](https://guacamole.apache.org/) is a clientless remote desktop gateway. It supports standard protocols like VNC, RDP, and SSH.
* [MeshCentral](https://www.meshcommander.com/meshcentral2) looks pretty advanced and supports multiple operating systems.
* [Remotely](https://remotely.one/) provides remote desktop access and other advanced and management features.
* [RustDesk](https://github.com/rustdesk/rustdesk) is designed for zero-configuration ease of use and you can use their "rendezvous/relay server, set up your own, or write your own rendezvous/relay server"
  * Hacker News [thread](https://news.ycombinator.com/item?id=31456007)
* [Sshwift](https://github.com/nirui/sshwifty) is a SSH and Telnet connector self-hostable and served over the Internet, accessed via web browser
* [VNC](https://en.wikipedia.org/wiki/Virtual_Network_Computing) clients for remote desktop access.

### screenshots/screencasts/live streaming/annotation

* [Boltstream](https://github.com/benwilber/boltstream) self-hosted live video streaming website + backend
  * Hacker News [thread](https://news.ycombinator.com/item?id=25328622)
  * Reddit [thread](https://old.reddit.com/r/selfhosted/comments/k8ftp8/boltstream_selfhosted_full_endtoend_live_video/)
* [Flameshot](https://github.com/flameshot-org/flameshot) screenshot software with [Wayland](https://en.wikipedia.org/wiki/Wayland_(display_server_protocol)) support
* [Kooha](https://apps.gnome.org/en/app/io.github.seadve.Kooha/) is a Gnome simple screen recorder with a minimalist interface
  * Record microphone, desktop audio, or both at the same time
  * Support for WebM, MP4, GIF, and Matroska formats
  * Select a monitor, a window, or a portion of the screen to record
* [menyoki](https://github.com/orhun/menyoki) for screenshots and screencasts operated from the command line as "ImageOps"
  * Reddit [thread](https://old.reddit.com/r/linux/comments/k8k5ut/menyoki_screenshotcast_and_perform_imageops_on/)
* [Peek](https://github.com/phw/peek) simple screen recorder
* [Pensela](https://github.com/weiameili/Pensela) Swiss Army Knife of screen annotation tools
* [Project Lightspeed](https://github.com/GRVYDEV/Project-Lightspeed)
* [OBS Studio](https://obsproject.com/) is one of the leading screencasting, video recording, and live streaming software and cross platform
* [Satyr](https://git.waldn.net/git/knotteye/satyr) self-hosted live streaming server
* [Sorbay](https://github.com/sorbayhq/sorbay) for recording screen, camera, and microphone with self-hostable server for sharing links to recordings
  * Hacker News [thread](https://news.ycombinator.com/item?id=35042662)

### search engines

E.g. Google.

Some of these are unfortunately not fully free software, but IMO still better than the likes of Google.

* [Marginalia](https://search.marginalia.nu/) "is an independent DIY search engine that focuses on non-commercial content, and attempts to show you sites you perhaps weren't aware of in favor of the sort of sites you probably already knew existed"
  * Hacker News thread [here](https://news.ycombinator.com/item?id=31536626) and [here](https://news.ycombinator.com/item?id=28550764) 
* [StartPage](https://www.startpage.com/)
* [searx](https://searx.me/)
* [ixQuick](https://www.ixquick.eu/)
* [DuckDuckGo](https://duckduckgo.com/) (not perfect but still much better than Google)
* Qwant?
* [wiby](http://wiby.me/) is a retro search engine that works with vintage computers later released as [self-hostable free software](https://github.com/wibyweb/wiby)

### speech

I.e. text-to-speech (TTS) or speech-to-text (STT)

* [Bark](https://github.com/suno-ai/bark) is a transformer-based text-to-audio model created by Suno. Bark can generate highly realistic, multilingual speech as well as other audio - including music, background noise and simple sound effects.
  * Hacker News [thread](https://news.ycombinator.com/item?id=35943108)
  * [Bark Infinity](https://github.com/JonathanFly/bark) commandline wrapper with CPU offloading and other niceties
* [Coqui TTS](https://github.com/coqui-ai/TTS) is a continuation of the [Mozilla TTS](https://github.com/mozilla/TTS) project which is partly based on the Mozilla Common Voice dataset and others
  * Install an instance of their demo server to convert text to speech
  * Hacker News [thread](https://news.ycombinator.com/item?id=26810079) and [thread](https://news.ycombinator.com/item?id=26790951) with more resources
* [ESPnet](https://github.com/espnet/espnet) is an end-to-end speech processing toolkit covering end-to-end speech recognition, text-to-speech, speech translation, speech enhancement, speaker diarization, spoken language understanding, and so on.
  * [Usage example](https://neuml.hashnode.dev/text-to-speech-generation)
* [Live Captions](https://github.com/abb128/LiveCaptions) is an application that provides live captions for the Linux desktop
  * Hacker News [thread](https://news.ycombinator.com/item?id=34034121)
* [Mimic](https://mycroft.ai/initiatives/#mimic) is "Mycroft’s new privacy-focused neural Text to Speech (TTS) engine. In human terms that means it sounds great, and can run completely offline. Mimic 3 has over 100 different voices and can speak more than a dozen languages."
* [Nerd Dictation](https://github.com/ideasman42/nerd-dictation) offline speech-to-text dictation tool
  * Hacker News [thread](https://news.ycombinator.com/item?id=29972579)
* [opentts](https://github.com/synesthesiam/opentts) "unifies access to multiple open source text to speech systems and voices for many languages"
* [oTranscribe](https://github.com/oTranscribe/oTranscribe) is a free web app designed to take the pain out of transcribing recorded interviews
* [pyTranscribe](https://github.com/raryelcostasouza/pyTranscriber) is an automatic speech transcription and subtitling program
  * Sadly seems to use Google's speech recognition API for now as backend
* [Rhasspy](https://rhasspy.readthedocs.io/en/latest/) is an open source, fully offline set of voice assistant services for many human languages
  * It includes the [Larynx](https://github.com/rhasspy/larynx) server which can run independently as a TTS service ([samples](https://rhasspy.github.io/larynx/) which sound good but still kind of robotic)
* [Vosk](https://alphacephei.com/vosk/) is a multi-language speech recognition toolkit
* [Whisper](https://github.com/openai/whisper/) "is a general-purpose speech recognition model. It is trained on a large dataset of diverse audio and is also a multi-task model that can perform multilingual speech recognition as well as speech translation and language identification."
  * Reddit [thread](https://old.reddit.com/r/Python/comments/xqlay2/speech_to_text_that_actually_works_my_first/)
  * [C++ implementation](https://github.com/ggerganov/whisper.cpp) that could be much faster
  * Online [transcription service](https://news.ycombinator.com/item?id=33663486) using Whisper

### spelling and grammar checker

E.g. Grammerly

* [Language Tool](https://languagetool.org)
  * Unfortunately doesn't seem to be fully free software

### spreadsheets

E.g. Google Sheets or Microsoft Excel.

* [EtherCalc](https://ethercalc.org/)
* [Luckysheet](https://github.com/mengshukeji/Luckysheet)
* [Teapot](https://www.syntax-k.de/projekte/teapot/) "Type-Safe 3D Spreadsheet"
  * Hacker News [thread](https://news.ycombinator.com/item?id=29241136)

### surveys

E.g. Surveymonkey or Google Forms

* [LimeSurvey](https://www.limesurvey.org/)
* [Journey](http://welcome.journeysurveys.com/)
* [Framaforms](https://framaforms.org/)
* [EU Survey](https://ec.europa.eu/eusurvey/) (made by the EU Comission)
* [OhMyForm](https://github.com/ohmyform/ohmyform)
* [SurveyStack](https://app.surveystack.io/)
  * Used for GOSH 2022 election listening survey
  * [Announcement post](https://www.our-sci.net/introducing_surveystack/)
  * Also used for [data submission](https://www.our-sci.net/a-day-in-the-life-of-a-data-point/) (wam)
  * [Example survey](https://bionutrient.surveystack.io/) by the Bionutrient Food Association 
  * Can be tied into a [dashboard](https://app.surveystack.io/static/pages/dataExplorer?explained=polyphenols)

### tasks/todo list management (Kanban)

E.g. Trello.

* [Focalboard](https://www.focalboard.com/)
  * Has general project management features in addition to task board
  * Hacker News [thread](https://news.ycombinator.com/item?id=26499062)
  * Reddit [thread](https://old.reddit.com/r/selfhosted/comments/m72mes/focalboard_open_source_selfhosted_project/)
* [Grit](https://github.com/climech/grit) is a multitree-based personal task manager
  * Hacker News [thread](https://news.ycombinator.com/item?id=26673221)
* [Personal Kanban](https://personalkanban.js.org/)
  * Hacker News [thread](https://news.ycombinator.com/item?id=25430325) with other suggestions and discussion
* [Planka](https://github.com/plankanban/planka)
* [Taiga](https://tree.taiga.io/discover)
* [Taskcafé](https://github.com/JordanKnott/taskcafe)
* [Vikunja](https://vikunja.io/) is a self-hostable todo list app with kanban features and dependency management for tasks
  * Hacker News [thread](https://news.ycombinator.com/item?id=34469078)
* [Nextcloud](https://www.nextcloud.org/) has a Kanban board app
* [Super Producitvity](https://super-productivity.com/) task based Jira/github/gitlab integration, time tracking, great features

### text editors

* [EncryptPad](https://github.com/evpo/EncryptPad) is a text editor and binary encryptor that implements RFC 4880 Open PGP format: symmetrically encrypted, compressed and integrity protected
* [Espanso](https://github.com/espanso/espanso) is cross-platform a text expander that detects when you type a specific keyword and replaces it with something else
  * Hacker News [thread](https://news.ycombinator.com/item?id=32605073)

### time tracking/management

* [Pomotroid](https://github.com/Splode/pomotroid) is a cross platform and featureful Pomodoro timer
* [Time Cop](https://timecop.app/en/) is an Apache-2.0 licensed mobile app for time tracking
  * [Hacker News](https://news.ycombinator.com/item?id=22748242) discussion

### translation

* [Bergamot](https://browser.mt/) is an under-development "private, client-side machine translation in a web browser. An open source consortium involving University of Edinburgh, Charles University in Prague, the University of Sheffield, University of Tartu, and Mozilla."
  * Available as Firefox [extension](https://addons.mozilla.org/en-US/firefox/addon/firefox-translations/)
  * Also as online [instance](https://mozilla.github.io/translate/)
* [LibreTranslate](https://github.com/uav4geo/LibreTranslate/) provides a machine translation API with no reliance on any proprietary service plus a web interface
  * [Live instance](https://libretranslate.com/)
  * Hacker News [thread](https://news.ycombinator.com/item?id=26048800)

### two-factor authentication (2FA)

* [andOTP](https://github.com/andOTP/andOTP)
* [Aegis](https://github.com/beemdevelopment/Aegis)
* [FreeOTP+](https://github.com/helloworld1/FreeOTPPlus)

### URL/link shorteners

* [Framalink](https://frama.link/)
* [1n.pm](https://1n.pm/)
* [PURL](https://purl.archive.org)
  * Managed by the [Internet Archive](https://archive.org) for long-term persistence
  * Flexible shortened URL management features
* There are many others or set up your own with [YOURLS](https://yourls.org/)

### user interface (UI) & user experience (UX) mockups

* [3D Book Image CSS Generator](https://github.com/scastiel/3d-book-image-css-generator)
  * Hacker News [thread](https://news.ycombinator.com/item?id=23896856)
* [Animockup](https://animockup.com/)
  * Hacker News [thread](https://news.ycombinator.com/item?id=22647445)
* [Screenshot.rocks](https://github.com/daveearley/screenshot.rocks) to create browser mockups from screenshots
  * Hacker News [thread](https://news.ycombinator.com/item?id=23731531)

### video editors

E.g. Apple iMovie, Apple Final Cut, or Adobe Premiere.

* [Blender](https://www.blender.org/) (very advanced)
  * Also used for 3D rendering
* [editly](https://github.com/mifi/editly) "allows you to easily and programmatically create a video from a set of clips, images, audio and titles, with smooth transitions and music overlaid"
* [Kdenlive](https://kdenlive.org/en/)
* [Moonray](https://openmoonray.org/download) is the production 3D renderer used by DreamwWorks
  * Hacker News [thread](https://news.ycombinator.com/item?id=32450386)
* OBS Studio
  * [OCI container of OBS Studio](https://github.com/ublue-os/obs-studio-portable) with 50 plugins included
    * Hacker News [thread](https://news.ycombinator.com/item?id=37777362)
* [OpenShot](http://openshot.org/) (very actively developed)
* Shotcut

### web analytics/monitoring

E.g. Google Analytics.

See [this post on LWN](https://lwn.net/Articles/822568/) and [this blog post](https://blog.elementary.io/leaving-google-analytics-is-finally-plausible/) by [Elementary OS](https://elementary.io/) that describes the problem with non-free analystics very well and what to consider when implementing analytics.

* [Ackee](https://ackee.electerious.com/) is a "self-hosted, Node.js based analytics tool for those who care about privacy".
* [freshlytics](https://github.com/sheshbabu/freshlytics)
* [GoatCounter](https://www.goatcounter.com/)
* "[GoAccess](https://goaccess.io/) is an open source real-time web log analyzer" that is light weight, less intrusive, and can replace full-blown analytics packages
* [Plausible](https://plausible.io/) is fully free software. Of note:
  * They enable and encourage you to present your analytics dashboard to visitors of your website.
  * They don't accept VC funding with [interesting post on making money with open source software](https://plausible.io/blog/open-source-funding).
  * Automatically compliant with GDPR with no need for cookie banners!
* [Shynet](https://github.com/milesmcc/shynet), "minimal" and free software web analytics
  * [Reddit threads](https://old.reddit.com/r/selfhosted/duplicates/g79ojw/shynet_an_open_source_web_analytics_tool_thats/)
* [statping](https://github.com/statping/statping) is an "easy to use Status Page for your websites and applications. Statping will automatically fetch the application and render a beautiful status page with tons of features for you to build an even better status page."
* [Umami](https://umami.is/)
  * [Hacker News thread](https://news.ycombinator.com/item?id=24198329)
* Top 4 open source [alternatives to Google Analytics](https://opensource.com/article/18/1/top-4-open-source-analytics-tools)
* Additional discussion in [this Hacker News thread](https://news.ycombinator.com/item?id=29888599) with more suggestions

### web browser

E.g. Microsoft Internet Explorer, Microsoft Edge, Apple Safari, or Google Chrome.

* Chromium
  * Chromium is open source but, by default, includes lots of intrusive Google integrations.
  * [Bromite](https://www.bromite.org/) privacy-enhanced Chromium fork for Android 
    * Hacker News [thread](https://news.ycombinator.com/item?id=26135559)
  * [ungoogled-chromium](https://github.com/ungoogled-software/ungoogled-chromium) is built from Chromium source code sans dependency on Google web services plus additional privacy tweaks.
  * [chrlauncher](https://github.com/henrypp/chrlauncher) is an updater and launcher for various builds of Chromium, including ungoogled-chromium.
* [MicroWeb](https://github.com/jhhoward/MicroWeb) is a "DOS web browser for 8088 class machines"
* [Mozilla Firefox](https://www.mozilla.org/firefox/)
* [TxtNet Browser](https://github.com/lukeaschenbrenner/TxtNet-Browser/) is an Android app that allows anyone around the world to browse the web without a mobile data connection! It uses SMS as a medium of transmitting HTTP requests
  * Hacker News [thread](https://news.ycombinator.com/item?id=35660796)

### word processing

E.g. Google Sheets or Microsoft Excel.

* [CodiMD](https://github.com/codimd/server) provides "realtime collaborative markdown notes on all platforms."
* [EtherPad](http://etherpad.org/)